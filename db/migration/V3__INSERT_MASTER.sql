insert into purpose(purpose_id, message) values(1, 'フレンド');
insert into purpose(purpose_id, message) values(2, '対戦');
insert into purpose(purpose_id, message) values(3, 'COOP');
insert into purpose(purpose_id, message) values(4, '実績');
insert into purpose(purpose_id, message) values(5, 'その他');


insert into hardware(hardware_id, name) values(1, 'PS4');
insert into hardware(hardware_id, name) values(2, 'PS3');
insert into hardware(hardware_id, name) values(4, 'XboxOne');
insert into hardware(hardware_id, name) values(5, 'Xbox360');
insert into hardware(hardware_id, name) values(6, 'WiiU');
insert into hardware(hardware_id, name) values(7, '3DS');
insert into hardware(hardware_id, name) values(8, 'PS Vita');
