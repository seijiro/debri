create table purpose(
  purpose_id int PRIMARY KEY,
  message VARCHAR(255) not null
);

create table game(
  game_id serial primary key,
  title varchar(255) not null,
  hardwares varchar(255) not NULL
);

create table hardware(
  hardware_id int PRIMARY KEY,
  name VARCHAR(255) not NULL
);

CREATE TABLE rooms (
  room_id serial,
  title varchar(255) not null,
  game_id int not null REFERENCES game(game_id),
  purpose_id int not null REFERENCES purpose(purpose_id) ,
  play_time varchar(255) not null,
  player_capacity int not null,
  status int not null,
  vc_flag BOOL not null,
  display_day int not null CHECK (display_day > 0 and display_day <= 7),
  description text not null,
  created_at TIMESTAMP not null,
  updated_at TIMESTAMP not null,
  primary key(room_id)
);

CREATE TABLE room_attend (
  room_id INT NOT NULL,
  user_id int not null,
  status int not null,
  created_at timestamp not null,
  updated_at timestamp not null,
  PRIMARY KEY (room_id, user_id)
);

