CREATE TABLE users (
  user_id serial,
  user_name varchar(255) not null UNIQUE,
  profile text not null,
  thumbnail varchar(255) not null,
  email varchar(255),
  homepage varchar(255),
  password varchar(255) not null,
  created_at TIMESTAMP not null,
  updated_at TIMESTAMP not null,
  primary key(user_id)
);
