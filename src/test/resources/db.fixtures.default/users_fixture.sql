#!SetUp


truncate table users restart identity CASCADE;

insert into users(
  user_name,
  profile,
  thumbnail,
  email,
  homepage,
  password,
  created_at,
  updated_at)
values(
  'admin',
  'profile',
  'http://thumbnail.url',
  'test@email.com',
  'http://homepge.com',
  MD5('password'),
  now(),
  now());

insert into users(
  user_name,
  profile,
  thumbnail,
  email,
  homepage,
  password,
  created_at,
  updated_at)
values(
  'superAdmin',
  'profile',
  'http://thumbnail.url',
  'test@email.com',
  'http://homepge.com',
  MD5('password'),
  now(),
  now());

#!TearDown
