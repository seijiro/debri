#!SetUp
truncate table purpose restart identity CASCADE;


INSERT INTO purpose (purpose_id, message) VALUES (1, 'フレンド');
INSERT INTO purpose (purpose_id, message) VALUES (2, '対戦');
INSERT INTO purpose (purpose_id, message) VALUES (3, 'COOP');
INSERT INTO purpose (purpose_id, message) VALUES (4, '実績');
INSERT INTO purpose (purpose_id, message) VALUES (5, 'その他');

#!TearDown