#!SetUp
truncate table hardware restart identity CASCADE;

INSERT INTO hardware (hardware_id, name) VALUES (1, 'PS4');
INSERT INTO hardware (hardware_id, name) VALUES (2, 'PS3');
INSERT INTO hardware (hardware_id, name) VALUES (4, 'XboxOne');
INSERT INTO hardware (hardware_id, name) VALUES (5, 'Xbox360');
INSERT INTO hardware (hardware_id, name) VALUES (6, 'WiiU');
INSERT INTO hardware (hardware_id, name) VALUES (7, '3DS');
INSERT INTO hardware (hardware_id, name) VALUES (8, 'PS Vita');

#!TearDown