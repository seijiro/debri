package com.github.seijiro.controllers

import javax.servlet.http.HttpServletRequest

import com.github.seijiro.helpers.SetupDB
import com.github.seijiro.support.LoggerSupport
import org.scalatest.{ FunSpec, FunSuiteLike }
import org.scalatest.tags.Slow
import org.scalatra.test.scalatest._

import scala.util.Random

class AuthControllerForTest extends AuthController {
  val AUTH_COOKIE_NAME = "AUTH"
  override def isAuthenticated(implicit request: HttpServletRequest): Boolean =
    request.cookies.get(AUTH_COOKIE_NAME).isDefined
}

@Slow
class LoginControllerTest extends FunSpec with ScalatraSpec with SetupDB {
  addServlet(classOf[AuthControllerForTest], "/auth/*")

  val testUserName = Random.nextInt().toString
  def withUserSessionHeader(headers: Map[String, String] = Map.empty) = {
    headers + ("Cookie" -> s"USER=$testUserName;")
  }

  /**
   * 前提: username:admin, password:passwordのユーザが存在する
   */

  describe("register") {
  }

  describe("パスワードログイン") {
    describe("usernameのバリデーションのテスト") {
      it("4文字未満の文字を送ったら500番が戻る") {
        post("/auth/login", List("username" -> "bit", "password" -> "password")) {
          status should equal(500)
        }
      }

      /**
       * 間違えてるので/auth/loginにリダイレクトされる
       */
      it("4文字を送ったら302が戻る") {
        post("/auth/login", List("username" -> "bits", "password" -> "password")) {
          status should equal(302)
        }
      }

    }
    describe("passwordのバリデーションのテスト") {
      it("パスワードに5文字未満の文字を送ったら500番が戻る") {
        post("/auth/login", List("username" -> "admin", "password" -> "1234")) {
          status should equal(500)
        }
      }
      it("パスワードに5文字の文字を送ったら302が戻る.") {
        post("/auth/login", List("username" -> "12345", "password" -> "12345")) {
          status should equal(302)
        }
      }
    }

    it("ログイン処理.誤ったId/Passwordを入力すると、/loginにredirectされる") {
      post("/auth/login", List("username" -> "fxxk", "password" -> "fxxk.you")) {
        status should equal(302)
        header.get("Location").get should include("/login")
      }
    }

    it("ログイン処理.正しいId/Passwordを入力すると、/にredirectされる") {
      post("/auth/login", List("username" -> "admin", "password" -> "password")) {
        status should equal(302)

        /**
         * TODO テストとして甘い
         * $host:port/
         * にRedirectされたテストにしたい
         */
        val location = header.get("Location").get
        location should include("/")
        location should not include ("/auth/")
      }
    }
  }
}
