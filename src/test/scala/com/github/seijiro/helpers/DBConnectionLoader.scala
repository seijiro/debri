package com.github.seijiro.helpers

import com.typesafe.config.ConfigFactory

trait DBConnectionLoader {

  // https://github.com/typesafehub/config
  val config = ConfigFactory.load()
  val driver = config.getString("db.default.driver")
  val url = config.getString("db.default.url")
  val username = config.getString("db.default.user")
  val password = config.getString("db.default.password")

}
