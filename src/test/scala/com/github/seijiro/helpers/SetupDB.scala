package com.github.seijiro.helpers

import com.github.seijiro.support.LoggerSupport
import org.scalatest.{ BeforeAndAfter, Suite }
import com.github.tototoshi.fixture._
import scalikejdbc.ConnectionPool

trait SetupDB extends LoggerSupport
    with DBConnectionLoader
    with BeforeAndAfter {
  self: Suite =>

  protected val ddlFixture = Fixture(driver, url, username, password)
    .scriptLocation("db.fixtures.default")
    .scripts(Seq("users_fixture.sql", "game_fixture.sql", "purpose_fixture.sql", "hardware_fixture.sql", "room_fixture.sql"))

  def loadJDBCSettings() {
    ConnectionPool.singleton(url, username, password)
  }

  loadJDBCSettings()

  before {
    ddlFixture.setUp()
  }

  after {
    ddlFixture.tearDown()
  }

}
