package com.github.seijiro

import com.github.seijiro.models.repositories._
import com.github.seijiro.models.services._
import org.mockito.Mockito

/**
 * Created by ozw-sei on 2016/02/17.
 */
trait TestEnvironment
    extends UserRegisterServiceComponent
    with UserRepositoryComponent
    with UserFindServiceComponent
    with RoomRepositoryComponent {
  val userRepository = Mockito.mock(classOf[UserRepository])
  val userRegisterService = Mockito.mock(classOf[UserRegisterService])
  val userFindService = Mockito.mock(classOf[UserFindService])
  val roomRepository = Mockito.mock(classOf[RoomRepository])

}
