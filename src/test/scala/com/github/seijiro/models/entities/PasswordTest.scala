package com.github.seijiro.models.entities

import com.github.seijiro.infra.MD5Module
import org.scalatest._

class PasswordTest extends FunSuiteLike with MD5Module with ShouldMatchers {

  test("Password.isPassed(A, B) で左がPlane、右がEncryptedを渡し、合っていたらTrueが戻る") {
    val plane = "password"
    val encrypted = md5(plane)

    val actual = Password.isPassed(plane, encrypted)
    val expected = true

    actual should be(expected)
  }

  test("Password.isPassed(A, B) で関係ないものを渡すとFalseが戻る") {
    val plane = "password"
    val encrypted = md5("Batman")

    val actual = Password.isPassed(plane, encrypted)
    val expected = false

    actual should be(expected)
  }

}
