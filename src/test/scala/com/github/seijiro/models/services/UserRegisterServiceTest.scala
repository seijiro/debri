package com.github.seijiro.models.services

import com.github.seijiro.{ UserNameIsExistError, UserNotFoundError, TestEnvironment }
import com.github.seijiro.models.dao.Users
import com.github.seijiro.models.entities.{ Password, LoginUser }
import com.github.seijiro.models.repositories.UserRepositoryComponent
import com.github.seijiro.support.DatetimeSupport
import org.mockito.Mockito
import org.mockito.Mockito.{ when => W }
import org.scalatest._
import org.scalatest.mock.MockitoSugar._
import scalaz._
import Scalaz._

/**
 * Created by ozw-sei on 2016/02/17.
 */

class UserRegisterServiceTest extends FunSpec
    with BeforeAndAfter
    with TestEnvironment
    with Matchers
    with DatetimeSupport {
  override val userRegisterService = new UserRegisterService

  val userName = "userName"
  val incorrectUserName = "Hypagdfalkjsdflak"
  val createUserName = "createUserName"
  val password = "password"

  val existUserName = "pop"
  val newUserName = "new"
  val dummyUser = Users(1, userName, "profile", "thumbnail", None, None, Password.createFromPlane(password).toString, n, n)
  val newLoginUser = LoginUser(dummyUser)

  val n = now

  before {
    val dummyUser = Users(1, userName, "profile", "thumbnail", None, None, Password.createFromPlane(password).toString, n, n)

    val login: LoginUser = LoginUser(dummyUser)
    W(userFindService.findByUserName(userName)).thenReturn(Some(login))
    W(userFindService.findByUserName(incorrectUserName)).thenReturn(None)

    val newUser = Users(2, newUserName, "profile", "thumbnail", None, None, Password.createFromPlane(password).toString, n, n)
    W(userRepository.findByUserName(existUserName)).thenReturn(Some(dummyUser))
    W(userRepository.findByUserName(newUserName)).thenReturn(None)
    W(userRepository.create(newUserName, Password.createFromPlane(password).toString,
      "", "",
      None, None)).thenReturn(newUser)
  }

  describe("UserRegisterService.verify.") {

    it("verify.存在しないユーザ名を入れた場合はLeft(UserNotFoundError)が戻る") {
      val actual = userRegisterService.verify(incorrectUserName, password)
      val expected = -\/(UserNotFoundError)
      actual should be equals (expected)
    }

    it("verify.存在するユーザで正しいId/passwordを入れた場合はRight(true)が戻る") {
      val actual = userRegisterService.verify(userName, password)
      val expected = \/-(true)
      actual should be equals (expected)
    }

    it("verify.存在するユーザで不正なId/passwordを入れた場合はRight(false)が戻る") {
      val actual = userRegisterService.verify(userName, "HyperPlanePassword")
      val expected = \/-(false)
      actual should be equals (expected)
    }
  }

  describe("UserRegisterService.register") {

    it("register.存在するユーザ名を入力した場合はLeft(UserNameIsExistError)が戻る") {
      val actual = userRegisterService.register(existUserName, password)
      val expected = -\/(UserNameIsExistError)
      actual should be equals (expected)
    }

    it("register.存在しないユーザ名を入力した場合はRight(LoginUser)が戻る") {
      val actual = userRegisterService.register(newUserName, password)
      val expected = \/-(newLoginUser)
      actual should be equals (expected)
    }

  }

}
