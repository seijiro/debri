package com.github.seijiro.models.dao

import org.scalatest._
import scalikejdbc.scalatest.AutoRollback
import scalikejdbc._

class GameSpec extends fixture.FlatSpec with Matchers with AutoRollback {

  behavior of "Game"

  //  it should "find by primary keys" in { implicit session =>
  //    val maybeFound = Game.find(123)
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find by where clauses" in { implicit session =>
  //    val maybeFound = Game.findBy(sqls"game_id = ${123}")
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find all records" in { implicit session =>
  //    val allResults = Game.findAll()
  //    allResults.size should be >(0)
  //  }
  //  it should "count all records" in { implicit session =>
  //    val count = Game.countAll()
  //    count should be >(0L)
  //  }
  //  it should "find all by where clauses" in { implicit session =>
  //    val results = Game.findAllBy(sqls"game_id = ${123}")
  //    results.size should be >(0)
  //  }
  //  it should "count by where clauses" in { implicit session =>
  //    val count = Game.countBy(sqls"game_id = ${123}")
  //    count should be >(0L)
  //  }
  //  it should "create new record" in { implicit session =>
  //    val created = Game.create(title = "MyString", hardwares = "MyString")
  //    created should not be(null)
  //  }
  //  it should "save a record" in { implicit session =>
  //    val entity = Game.findAll().head
  //    // TODO modify something
  //    val modified = entity
  //    val updated = Game.save(modified)
  //    updated should not equal(entity)
  //  }
  //  it should "destroy a record" in { implicit session =>
  //    val entity = Game.findAll().head
  //    Game.destroy(entity)
  //    val shouldBeNone = Game.find(123)
  //    shouldBeNone.isDefined should be(false)
  //  }
  //  it should "perform batch insert" in { implicit session =>
  //    val entities = Game.findAll()
  //    entities.foreach(e => Game.destroy(e))
  //    val batchInserted = Game.batchInsert(entities)
  //    batchInserted.size should be >(0)
  //  }
}
