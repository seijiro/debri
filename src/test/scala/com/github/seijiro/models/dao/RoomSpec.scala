package com.github.seijiro.models.dao

import org.scalatest._
import scalikejdbc.scalatest.AutoRollback
import scalikejdbc._
import org.joda.time.{ DateTime }

class RoomSpec extends fixture.FlatSpec with Matchers with AutoRollback {

  behavior of "Room"

  //  it should "find by primary keys" in { implicit session =>
  //    val maybeFound = Room.find(123)
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find by where clauses" in { implicit session =>
  //    val maybeFound = Room.findBy(sqls"room_id = ${123}")
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find all records" in { implicit session =>
  //    val allResults = Room.findAll()
  //    allResults.size should be >(0)
  //  }
  //  it should "count all records" in { implicit session =>
  //    val count = Room.countAll()
  //    count should be >(0L)
  //  }
  //  it should "find all by where clauses" in { implicit session =>
  //    val results = Room.findAllBy(sqls"room_id = ${123}")
  //    results.size should be >(0)
  //  }
  //  it should "count by where clauses" in { implicit session =>
  //    val count = Room.countBy(sqls"room_id = ${123}")
  //    count should be >(0L)
  //  }
  //  it should "create new record" in { implicit session =>
  //    val created = Room.create(title = "MyString", gameId = 123, purposeId = 123, playTime = "MyString", playerCapacity = 123, status = 123, vcFlag = false, displayDay = 123, description = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
  //    created should not be(null)
  //  }
  //  it should "save a record" in { implicit session =>
  //    val entity = Room.findAll().head
  //    // TODO modify something
  //    val modified = entity
  //    val updated = Room.save(modified)
  //    updated should not equal(entity)
  //  }
  //  it should "destroy a record" in { implicit session =>
  //    val entity = Room.findAll().head
  //    Room.destroy(entity)
  //    val shouldBeNone = Room.find(123)
  //    shouldBeNone.isDefined should be(false)
  //  }
  //  it should "perform batch insert" in { implicit session =>
  //    val entities = Room.findAll()
  //    entities.foreach(e => Room.destroy(e))
  //    val batchInserted = Room.batchInsert(entities)
  //    batchInserted.size should be >(0)
  //  }
}
