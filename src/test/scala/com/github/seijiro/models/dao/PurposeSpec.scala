package com.github.seijiro.models.dao

import org.scalatest._
import scalikejdbc.scalatest.AutoRollback
import scalikejdbc._

class PurposeSpec extends fixture.FlatSpec with Matchers with AutoRollback {

  behavior of "Purpose"

  //  it should "find by primary keys" in { implicit session =>
  //    val maybeFound = Purpose.find(123)
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find by where clauses" in { implicit session =>
  //    val maybeFound = Purpose.findBy(sqls"purpose_id = ${123}")
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find all records" in { implicit session =>
  //    val allResults = Purpose.findAll()
  //    allResults.size should be >(0)
  //  }
  //  it should "count all records" in { implicit session =>
  //    val count = Purpose.countAll()
  //    count should be >(0L)
  //  }
  //  it should "find all by where clauses" in { implicit session =>
  //    val results = Purpose.findAllBy(sqls"purpose_id = ${123}")
  //    results.size should be >(0)
  //  }
  //  it should "count by where clauses" in { implicit session =>
  //    val count = Purpose.countBy(sqls"purpose_id = ${123}")
  //    count should be >(0L)
  //  }
  //  it should "create new record" in { implicit session =>
  //    val created = Purpose.create(purposeId = 123, message = "MyString")
  //    created should not be(null)
  //  }
  //  it should "save a record" in { implicit session =>
  //    val entity = Purpose.findAll().head
  //    // TODO modify something
  //    val modified = entity
  //    val updated = Purpose.save(modified)
  //    updated should not equal(entity)
  //  }
  //  it should "destroy a record" in { implicit session =>
  //    val entity = Purpose.findAll().head
  //    Purpose.destroy(entity)
  //    val shouldBeNone = Purpose.find(123)
  //    shouldBeNone.isDefined should be(false)
  //  }
  //  it should "perform batch insert" in { implicit session =>
  //    val entities = Purpose.findAll()
  //    entities.foreach(e => Purpose.destroy(e))
  //    val batchInserted = Purpose.batchInsert(entities)
  //    batchInserted.size should be >(0)
  //  }
}
