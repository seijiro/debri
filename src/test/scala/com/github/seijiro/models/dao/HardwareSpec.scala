package com.github.seijiro.models.dao

import org.scalatest._
import scalikejdbc.scalatest.AutoRollback
import scalikejdbc._

class HardwareSpec extends fixture.FlatSpec with Matchers with AutoRollback {

  behavior of "Hardware"

  //  it should "find by primary keys" in { implicit session =>
  //    val maybeFound = Hardware.find(123)
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find by where clauses" in { implicit session =>
  //    val maybeFound = Hardware.findBy(sqls"hardware_id = ${123}")
  //    maybeFound.isDefined should be(true)
  //  }
  //  it should "find all records" in { implicit session =>
  //    val allResults = Hardware.findAll()
  //    allResults.size should be >(0)
  //  }
  //  it should "count all records" in { implicit session =>
  //    val count = Hardware.countAll()
  //    count should be >(0L)
  //  }
  //  it should "find all by where clauses" in { implicit session =>
  //    val results = Hardware.findAllBy(sqls"hardware_id = ${123}")
  //    results.size should be >(0)
  //  }
  //  it should "count by where clauses" in { implicit session =>
  //    val count = Hardware.countBy(sqls"hardware_id = ${123}")
  //    count should be >(0L)
  //  }
  //  it should "create new record" in { implicit session =>
  //    val created = Hardware.create(hardwareId = 123, name = "MyString")
  //    created should not be(null)
  //  }
  //  it should "save a record" in { implicit session =>
  //    val entity = Hardware.findAll().head
  //    // TODO modify something
  //    val modified = entity
  //    val updated = Hardware.save(modified)
  //    updated should not equal(entity)
  //  }
  //  it should "destroy a record" in { implicit session =>
  //    val entity = Hardware.findAll().head
  //    Hardware.destroy(entity)
  //    val shouldBeNone = Hardware.find(123)
  //    shouldBeNone.isDefined should be(false)
  //  }
  //  it should "perform batch insert" in { implicit session =>
  //    val entities = Hardware.findAll()
  //    entities.foreach(e => Hardware.destroy(e))
  //    val batchInserted = Hardware.batchInsert(entities)
  //    batchInserted.size should be >(0)
  //  }
}
