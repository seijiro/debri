package com.github.seijiro.models.dao

import com.github.seijiro.helpers.SetupDB
import org.scalatest._
import scalikejdbc.scalatest.AutoRollback
import scalikejdbc._
import org.joda.time.DateTime

class UsersSpec extends fixture.FlatSpec
    with Matchers
    with AutoRollback
    with SetupDB {

  behavior of "Users"

  it should "find by primary keys" in { implicit session =>
    val maybeFound = Users.find(1)
    maybeFound.isDefined should be(true)
  }
  it should "find by where clauses" in { implicit session =>
    val maybeFound = Users.findBy(sqls"user_id = ${1}")
    maybeFound.isDefined should be(true)
  }
  it should "find all records" in { implicit session =>
    val allResults = Users.findAll()
    allResults.size should be > (0)
  }
  it should "count all records" in { implicit session =>
    val count = Users.countAll()
    count should be > (0L)
  }
  it should "find all by where clauses" in { implicit session =>
    val results = Users.findAllBy(sqls"user_id = ${1}")
    results.size should be > (0)
  }
  it should "count by where clauses" in { implicit session =>
    val count = Users.countBy(sqls"user_id = ${1}")
    count should be > (0L)
  }
  it should "create new record" in { implicit session =>
    val created = Users.create(userName = "admin2", profile = "MyString", thumbnail = "MyString", password = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
    created should not be (null)
  }
  it should "save a record" in { implicit session =>
    val entity = Users.findAll().head
    val modified = entity.copy(userName = "*******newUser**********")
    val updated = Users.save(modified)
    updated should not equal (entity)
  }

}
