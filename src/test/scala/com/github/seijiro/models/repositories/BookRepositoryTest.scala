//package com.github.seijiro.models.repositories
//
//import com.github.seijiro.helpers.SetupDB
//import com.github.seijiro.models.dao.{ Books, Users }
//import com.github.seijiro.{ DBTest, TestEnvironment }
//import org.joda.time.DateTime
//import org.scalatest._
//import scalikejdbc.scalatest._
//
//class BookRepositoryTest extends fixture.FunSpec
//    with Matchers
//    with AutoRollback
//    with SetupDB
//    with TestEnvironment {
//
//  override val bookRepository = new BookRepository
//
//  describe("findById") {
//    it("存在するユーザの場合はSome") { implicit session =>
//      val actual = bookRepository.findById(1).isDefined
//      val expected = true
//      actual should be equals expected
//    }
//
//    it("存在しない場合はNone") { implicit session =>
//      val actual = bookRepository.findById(9999999).isDefined
//      val expected = false
//      actual should be equals expected
//    }
//  }
//
//  describe("findLatestWithPaging") {
//    it("Paging(1, 1)を渡すと最新2件目を返す") { implicit session =>
//      val paging = Paging(1, 1)
//      // 2.sqlの2番めに新しいのは10でなければならない
//      val expectedBookId = 10
//      val actual = bookRepository.findLatestWithPaging(paging)
//      actual.head.bookId should be equals (expectedBookId)
//    }
//
//  }
//
//}
