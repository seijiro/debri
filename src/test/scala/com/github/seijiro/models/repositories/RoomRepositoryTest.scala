package com.github.seijiro.models.repositories

import com.github.seijiro.{ DBTest, TestEnvironment }
import com.github.seijiro.helpers.SetupDB
import org.scalatest.Matchers
import scalikejdbc.scalatest.AutoRollback
import org.scalatest._
import scalikejdbc.scalatest._

class RoomRepositoryTest extends fixture.FunSpec with Matchers
    with AutoRollback
    with SetupDB
    with TestEnvironment {

  override val roomRepository = new RoomRepository

  describe("findRecentPost") {
    it("Take数", DBTest) { implicit session =>
      val actual = roomRepository.findRecentPost(Paging(2, 2))
      assert(actual._1.length == 2)
    }
    it(" 取得可能な件数がTakeよりも少ない場合は取れる限りの値を取得する", DBTest) {
      implicit session =>

        // 最大件数を取得
        val count = roomRepository.countAll
        val take = 100
        val left = 10

        val actualLeft = roomRepository.findRecentPost(Paging(take, count.toInt - left))
        assert(actualLeft._1.length == left)
        assert(actualLeft._2.hasNext == false)
    }
    it("Skip数.hasPrev/hasNext", DBTest) { implicit session =>
      val actual = roomRepository.findRecentPost(Paging(2, 2))
      // SkipされるべきおのがSkipされている
      val expect = roomRepository.findRecentPost(Paging(4, 0))
      assert(actual._1 == expect._1.drop(2))
      assert(actual._2.hasPrev)
      assert(actual._2.hasNext)

    }
  }

}
