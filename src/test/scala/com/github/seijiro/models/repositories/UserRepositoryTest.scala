package com.github.seijiro.models.repositories

import com.github.seijiro.models.dao.Users
import com.github.seijiro.{ DBTest, TestEnvironment }
import com.github.seijiro.helpers.SetupDB
import org.scalatest._
import scalikejdbc.scalatest._

class UserRepositoryTest extends fixture.FlatSpec
    with Matchers
    with AutoRollback
    with SetupDB
    with TestEnvironment {
  behavior of "UserRepository"

  /**
   * TestEnvironmentですべてのComponentがMockになるので
   * Mockしたくないクラスは事前にnewする
   */
  override val userRepository = new UserRepository

  /**
   * DBテストには必ずDBTestタグをつける
   */
  it should "Find admin user DBに指定された名前(unique)で保存されているユーザが1件戻る" taggedAs (DBTest) in { implicit session =>
    val actual = userRepository.findByUserName("admin").isDefined
    val expected = true
    actual should be(expected)
  }

  it should "testFindByUserName 登録されていないユーザの場合はNoneが帰る" taggedAs (DBTest) in { implicit session =>
    val actual = userRepository.findByUserName("anonymus").isEmpty
    val expected = true
    actual should be(expected)
  }

  it should "testCreateWithUserName DBにレコードが保存される。同じユーザが戻る" taggedAs (DBTest) in { implicit session =>
    val name = "super_hacker"
    val createdUser: Users = userRepository.create(name, "password", "thumb", "profile", None, None)
    val testFind: Option[Users] = userRepository.findByUserName(name)

    testFind.isDefined should be(true)

    if (testFind.isEmpty) {
      fail("進行不能になりました")
    }

    createdUser.userId should be(testFind
      .map(_.userId).get)
    createdUser.userName should be(name)
  }
}
