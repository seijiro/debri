import com.github.seijiro.controllers.{ AuthController, HomeController }
import jp.sf.amateras.scalatra.forms.ValidationJavaScriptProvider
import org.scalatra._
import javax.servlet._

class ScalatraBootstrap extends LifeCycle with DBConnectionLoader {

  override def init(context: ServletContext) {

    context.mount(new HomeController, "/*")
    context.mount(new AuthController, "/auth/*")

    import scalikejdbc._

    ConnectionPool.singleton(url, username, password)

    /// logging ScalikeJdbc
    GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(
      enabled = true,
      logLevel = 'DEBUG,
      warningEnabled = true,
      warningThresholdMillis = 1000L,
      warningLogLevel = 'WARN
    )
  }
}
