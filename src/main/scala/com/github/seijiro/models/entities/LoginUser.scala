package com.github.seijiro.models.entities

import com.github.seijiro.infra.MD5Module
import com.github.seijiro.models.UserId
import com.github.seijiro.models.dao.Users

case class LoginUser(private val dto: Users) {
  lazy val userId: UserId = dto.userId
  lazy val userName: String = dto.userName
  lazy val email: Option[Email] = dto.email.map(Email)
  lazy val profile: Profile = Profile(dto.profile, dto.thumbnail, dto.homepage.map(Url))
  private val password: Password = Password(dto.password)

  def Verify(plane: String) = password.isPassed(plane)
}

case class Email(email: String)

case class Url(url: String)

case class Password(private val encrypted: String) {
  def isPassed(plane: String) = Password.isPassed(plane, encrypted)
  override def toString = encrypted
}
object Password extends MD5Module {
  def createFromPlane(plane: String) = Password(md5(plane))
  def isPassed(plane: String, encrypted: String) = md5(plane) == encrypted
}

case class Profile(profile: String, thumbnail: String, homepage: Option[Url])

