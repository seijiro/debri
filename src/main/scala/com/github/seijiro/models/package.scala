package com.github.seijiro

/**
 * Created by ozw-sei on 2016/02/03.
 */
package object models {
  type UserId = Int
  type RoomId = Int
}
