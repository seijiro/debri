package com.github.seijiro.models.services

import com.github.seijiro.models.UserId
import com.github.seijiro.models.entities.LoginUser
import com.github.seijiro.models.repositories.UserRepositoryComponent

trait UserFindServiceComponent {
  self: UserRepositoryComponent =>
  val userFindService: UserFindService

  class UserFindService {
    def findByUserName(userName: String): Option[LoginUser] =
      userRepository.findByUserName(userName).map(LoginUser)

    def findByUserId(userId: UserId): Option[LoginUser] =
      userRepository.findByUserId(userId).map(LoginUser)
  }

}

