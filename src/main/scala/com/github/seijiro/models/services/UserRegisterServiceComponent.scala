package com.github.seijiro.models.services

import com.github.seijiro.models.entities.{ LoginUser, Password }
import com.github.seijiro.models.repositories.UserRepositoryComponent
import com.github.seijiro._

import scalaz._
import Scalaz._

trait UserRegisterServiceComponent {
  self: UserRepositoryComponent with UserFindServiceComponent =>

  val userRegisterService: UserRegisterService

  class UserRegisterService {
    def register(userName: String, password: String): \/[Error, LoginUser] = {
      userRepository.findByUserName(userName) match {
        case Some(user) => UserNameIsExistError.left
        case None =>
          LoginUser(userRepository.create(userName, Password.createFromPlane(password).toString, "", "", None, None)).right
      }
    }

    def verify(userName: String, planePassword: String): \/[Error, Boolean] = {
      userFindService.findByUserName(userName) match {
        case Some(user) => user.Verify(planePassword).right
        case None => UserNotFoundError.left
      }
    }
  }
}
