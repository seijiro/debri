package com.github.seijiro.models.dao

import com.github.seijiro.models.repositories.Paging
import scalikejdbc._
import org.joda.time.{ DateTime }

case class Room(
  roomId: Int,
  title: String,
  gameId: Int,
  purposeId: Int,
  playTime: String,
  playerCapacity: Int,
  status: Int,
  vcFlag: Boolean,
  displayDay: Int,
  description: String,
  createdAt: DateTime,
  updatedAt: DateTime
)

object Room extends SQLSyntaxSupport[Room] {

  override val tableName = "rooms"

  override val columns = Seq("room_id", "title", "game_id", "purpose_id", "play_time", "player_capacity", "status", "vc_flag", "display_day", "description", "created_at", "updated_at")

  def apply(r: SyntaxProvider[Room])(rs: WrappedResultSet): Room = autoConstruct(rs, r)
  def apply(r: ResultName[Room])(rs: WrappedResultSet): Room = autoConstruct(rs, r)

  val r = Room.syntax("r")

  override val autoSession = AutoSession

  def find(roomId: Int)(implicit session: DBSession = autoSession): Option[Room] = {
    sql"""select ${r.result.*} from ${Room as r} where ${r.roomId} = ${roomId}"""
      .map(Room(r.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[Room] = {
    sql"""select ${r.result.*} from ${Room as r}""".map(Room(r.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Room.table}""".map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Room] = {
    sql"""select ${r.result.*} from ${Room as r} where ${where}"""
      .map(Room(r.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Room] = {
    sql"""select ${r.result.*} from ${Room as r} where ${where}"""
      .map(Room(r.resultName)).list.apply()
  }

  def findAllWithPaging(paging: Paging)(implicit session: DBSession = autoSession): List[Room] = {
    sql"""select ${r.result.*} from ${Room as r} order by ${r.updatedAt} desc offset ${paging.skip} limit ${paging.take}"""
      .map(Room(r.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Room as r} where ${where}"""
      .map(_.long(1)).single.apply().get
  }

  def create(
    title: String,
    gameId: Int,
    purposeId: Int,
    playTime: String,
    playerCapacity: Int,
    status: Int,
    vcFlag: Boolean,
    displayDay: Int,
    description: String,
    createdAt: DateTime,
    updatedAt: DateTime
  )(implicit session: DBSession = autoSession): Room = {
    val generatedKey = sql"""
      insert into ${Room.table} (
        ${column.title},
        ${column.gameId},
        ${column.purposeId},
        ${column.playTime},
        ${column.playerCapacity},
        ${column.status},
        ${column.vcFlag},
        ${column.displayDay},
        ${column.description},
        ${column.createdAt},
        ${column.updatedAt}
      ) values (
        ${title},
        ${gameId},
        ${purposeId},
        ${playTime},
        ${playerCapacity},
        ${status},
        ${vcFlag},
        ${displayDay},
        ${description},
        ${createdAt},
        ${updatedAt}
      )
      """.updateAndReturnGeneratedKey.apply()

    Room(
      roomId = generatedKey.toInt,
      title = title,
      gameId = gameId,
      purposeId = purposeId,
      playTime = playTime,
      playerCapacity = playerCapacity,
      status = status,
      vcFlag = vcFlag,
      displayDay = displayDay,
      description = description,
      createdAt = createdAt,
      updatedAt = updatedAt
    )
  }

  def batchInsert(entities: Seq[Room])(implicit session: DBSession = autoSession): Seq[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'title -> entity.title,
        'gameId -> entity.gameId,
        'purposeId -> entity.purposeId,
        'playTime -> entity.playTime,
        'playerCapacity -> entity.playerCapacity,
        'status -> entity.status,
        'vcFlag -> entity.vcFlag,
        'displayDay -> entity.displayDay,
        'description -> entity.description,
        'createdAt -> entity.createdAt,
        'updatedAt -> entity.updatedAt
      ))
    SQL("""insert into rooms(
        title,
        game_id,
        purpose_id,
        play_time,
        player_capacity,
        status,
        vc_flag,
        display_day,
        description,
        created_at,
        updated_at
      ) values (
        {title},
        {gameId},
        {purposeId},
        {playTime},
        {playerCapacity},
        {status},
        {vcFlag},
        {displayDay},
        {description},
        {createdAt},
        {updatedAt}
      )""").batchByName(params: _*).apply()
  }

  def save(entity: Room)(implicit session: DBSession = autoSession): Room = {
    sql"""
      update
        ${Room.table}
      set
        ${column.roomId} = ${entity.roomId},
        ${column.title} = ${entity.title},
        ${column.gameId} = ${entity.gameId},
        ${column.purposeId} = ${entity.purposeId},
        ${column.playTime} = ${entity.playTime},
        ${column.playerCapacity} = ${entity.playerCapacity},
        ${column.status} = ${entity.status},
        ${column.vcFlag} = ${entity.vcFlag},
        ${column.displayDay} = ${entity.displayDay},
        ${column.description} = ${entity.description},
        ${column.createdAt} = ${entity.createdAt},
        ${column.updatedAt} = ${entity.updatedAt}
      where
        ${column.roomId} = ${entity.roomId}
      """.update.apply()
    entity
  }

  def destroy(entity: Room)(implicit session: DBSession = autoSession): Unit = {
    sql"""delete from ${Room.table} where ${column.roomId} = ${entity.roomId}""".update.apply()
  }

}
