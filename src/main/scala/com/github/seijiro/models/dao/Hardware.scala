package com.github.seijiro.models.dao

import scalikejdbc._

case class Hardware(
  hardwareId: Int,
  name: String
)

object Hardware extends SQLSyntaxSupport[Hardware] {

  override val tableName = "hardware"

  override val columns = Seq("hardware_id", "name")

  def apply(h: SyntaxProvider[Hardware])(rs: WrappedResultSet): Hardware = autoConstruct(rs, h)
  def apply(h: ResultName[Hardware])(rs: WrappedResultSet): Hardware = autoConstruct(rs, h)

  val h = Hardware.syntax("h")

  override val autoSession = AutoSession

  def find(hardwareId: Int)(implicit session: DBSession = autoSession): Option[Hardware] = {
    sql"""select ${h.result.*} from ${Hardware as h} where ${h.hardwareId} = ${hardwareId}"""
      .map(Hardware(h.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[Hardware] = {
    sql"""select ${h.result.*} from ${Hardware as h}""".map(Hardware(h.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Hardware.table}""".map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Hardware] = {
    sql"""select ${h.result.*} from ${Hardware as h} where ${where}"""
      .map(Hardware(h.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Hardware] = {
    sql"""select ${h.result.*} from ${Hardware as h} where ${where}"""
      .map(Hardware(h.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Hardware as h} where ${where}"""
      .map(_.long(1)).single.apply().get
  }

  def create(
    hardwareId: Int,
    name: String
  )(implicit session: DBSession = autoSession): Hardware = {
    sql"""
      insert into ${Hardware.table} (
        ${column.hardwareId},
        ${column.name}
      ) values (
        ${hardwareId},
        ${name}
      )
      """.update.apply()

    Hardware(
      hardwareId = hardwareId,
      name = name
    )
  }

  def batchInsert(entities: Seq[Hardware])(implicit session: DBSession = autoSession): Seq[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'hardwareId -> entity.hardwareId,
        'name -> entity.name
      ))
    SQL("""insert into hardware(
        hardware_id,
        name
      ) values (
        {hardwareId},
        {name}
      )""").batchByName(params: _*).apply()
  }

  def save(entity: Hardware)(implicit session: DBSession = autoSession): Hardware = {
    sql"""
      update
        ${Hardware.table}
      set
        ${column.hardwareId} = ${entity.hardwareId},
        ${column.name} = ${entity.name}
      where
        ${column.hardwareId} = ${entity.hardwareId}
      """.update.apply()
    entity
  }

  def destroy(entity: Hardware)(implicit session: DBSession = autoSession): Unit = {
    sql"""delete from ${Hardware.table} where ${column.hardwareId} = ${entity.hardwareId}""".update.apply()
  }

}
