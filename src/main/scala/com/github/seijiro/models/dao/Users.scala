package com.github.seijiro.models.dao

import scalikejdbc._
import org.joda.time.{ DateTime }

private[models] case class Users(
  userId: Int,
  userName: String,
  profile: String,
  thumbnail: String,
  email: Option[String] = None,
  homepage: Option[String] = None,
  password: String,
  createdAt: DateTime,
  updatedAt: DateTime
)

object Users extends SQLSyntaxSupport[Users] {

  override val tableName = "users"

  override val columns = Seq("user_id", "user_name", "profile", "thumbnail", "email", "homepage", "password", "created_at", "updated_at")

  def apply(u: SyntaxProvider[Users])(rs: WrappedResultSet): Users = autoConstruct(rs, u)
  def apply(u: ResultName[Users])(rs: WrappedResultSet): Users = autoConstruct(rs, u)

  val u = Users.syntax("u")

  override val autoSession = AutoSession

  def find(userId: Int)(implicit session: DBSession = autoSession): Option[Users] = {
    sql"""select ${u.result.*} from ${Users as u} where ${u.userId} = ${userId}"""
      .map(Users(u.resultName)).single.apply()
  }

  def findByUserName(name: String)(implicit session: DBSession = autoSession): Option[Users] =
    sql"""select ${u.result.*} from ${Users as u} where ${u.userName} = $name""".map(Users(u.resultName)).single.apply

  def findAll()(implicit session: DBSession = autoSession): List[Users] = {
    sql"""select ${u.result.*} from ${Users as u}""".map(Users(u.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Users.table}""".map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Users] = {
    sql"""select ${u.result.*} from ${Users as u} where ${where}"""
      .map(Users(u.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Users] = {
    sql"""select ${u.result.*} from ${Users as u} where ${where}"""
      .map(Users(u.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Users as u} where ${where}"""
      .map(_.long(1)).single.apply().get
  }

  def create(
    userName: String,
    profile: String,
    thumbnail: String,
    email: Option[String] = None,
    homepage: Option[String] = None,
    password: String,
    createdAt: DateTime,
    updatedAt: DateTime
  )(implicit session: DBSession = autoSession): Users = {
    val generatedKey = sql"""
      insert into ${Users.table} (
        ${column.userName},
        ${column.profile},
        ${column.thumbnail},
        ${column.email},
        ${column.homepage},
        ${column.password},
        ${column.createdAt},
        ${column.updatedAt}
      ) values (
        ${userName},
        ${profile},
        ${thumbnail},
        ${email},
        ${homepage},
        ${password},
        ${createdAt},
        ${updatedAt}
      )
      """.updateAndReturnGeneratedKey.apply()

    Users(
      userId = generatedKey.toInt,
      userName = userName,
      profile = profile,
      thumbnail = thumbnail,
      email = email,
      homepage = homepage,
      password = password,
      createdAt = createdAt,
      updatedAt = updatedAt
    )
  }

  def save(entity: Users)(implicit session: DBSession = autoSession): Users = {
    sql"""
      update
        ${Users.table}
      set
        ${column.userName} = ${entity.userName},
        ${column.profile} = ${entity.profile},
        ${column.thumbnail} = ${entity.thumbnail},
        ${column.email} = ${entity.email},
        ${column.homepage} = ${entity.homepage},
        ${column.password} = ${entity.password},
        ${column.createdAt} = ${entity.createdAt},
        ${column.updatedAt} = ${entity.updatedAt}
      where
        ${column.userId} = ${entity.userId}
      """.update.apply()
    entity
  }

}
