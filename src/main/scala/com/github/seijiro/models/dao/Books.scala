//package com.github.seijiro.models.dao
//
//import com.github.tarao.bullet.{ Monad, HasA }
//import com.github.tarao.bullet.Implicits._
//import org.joda.time.DateTime
//import scalikejdbc._
//
//case class Books(
//  bookId: Int,
//  title: String,
//  description: String,
//  author: Option[String] = None,
//  isbn: String,
//  createdAt: DateTime
//)
//
//trait BookHasReviews extends HasA[Books, BookReview] {
//  def map(books: Seq[Books]): Seq[BookReview] = {
//    BookReview.findAllBy(sqls"book_id IN (${books.map(_.bookId)})")
//  }
//}
//object BookHasReviews extends BookHasReviews
//
//class ResolveBookReviews(book: Books) extends Monad.Resolve[Seq[BookReview], Books](book) {
//  override def run(ms: Seq[Monad.Resolve[Seq[BookReview], Books]]): Seq[Seq[BookReview]] = {
//    ms.map(_.value).map { car => BookReview.findAllBy(sqls"book_id = ${car.bookId}") }
//  }
//}
//
//object ResolveBookReviews {
//  def apply(book: Books): Monad.Resolve[Seq[BookReview], Books] = new ResolveBookReviews(book)
//}
//
//object Books extends SQLSyntaxSupport[Books] {
//
//  override val tableName = "books"
//
//  override val columns = Seq("book_id", "title", "description", "author", "isbn", "created_at")
//
//  def apply(b: SyntaxProvider[Books])(rs: WrappedResultSet): Books = autoConstruct(rs, b)
//  def apply(b: ResultName[Books])(rs: WrappedResultSet): Books = autoConstruct(rs, b)
//
//  val b = Books.syntax("b")
//
//  override val autoSession = AutoSession
//
//  def findLatestWithPaging(take: Int, skip: Int)(implicit session: DBSession = autoSession): Seq[Books] = {
//    sql"""
//         select ${b.result.*} from ${Books as b}
//         order by ${b.createdAt} desc
//         limit ${take} offset ${skip}
//      """.map(Books(b.resultName)).list.apply
//  }
//
//  def find(bookId: Int)(implicit session: DBSession = autoSession): Option[Books] = {
//    sql"""select ${b.result.*} from ${Books as b} where ${b.bookId} = ${bookId} """
//      .map(Books(b.resultName)).single.apply()
//  }
//
//  def findByAuthor(author: String)(implicit session: DBSession = autoSession) =
//    sql"""select ${b.result.*} from ${Books as b} where ${b.author} = ${author}"""
//      .map(Books(b.resultName)).list.apply
//
//  def create(
//    title: String,
//    description: String,
//    author: Option[String] = None,
//    isbn: String,
//    createdAt: DateTime
//  )(implicit session: DBSession = autoSession): Books = {
//    val generatedKey = sql"""
//      insert into ${Books.table} (
//        ${column.title},
//        ${column.description},
//        ${column.author},
//        ${column.isbn},
//        ${column.createdAt}
//      ) values (
//        ${title},
//        ${description},
//        ${author},
//        ${isbn},
//        ${createdAt}
//      )
//      """.updateAndReturnGeneratedKey.apply()
//
//    Books(
//      bookId = generatedKey.toInt,
//      title = title,
//      description = description,
//      author = author,
//      isbn = isbn,
//      createdAt
//    )
//  }
//
//  def save(entity: Books)(implicit session: DBSession = autoSession): Books = {
//    sql"""
//      update
//        ${Books.table}
//      set
//        ${column.bookId} = ${entity.bookId},
//        ${column.title} = ${entity.title},
//        ${column.description} = ${entity.description},
//        ${column.author} = ${entity.author}
//      where
//        ${column.bookId} = ${entity.bookId}
//      """.update.apply()
//    entity
//  }
//
//  def destroy(entity: Books)(implicit session: DBSession = autoSession): Unit = {
//    sql"""delete from ${Books.table} where ${column.bookId} = ${entity.bookId}""".update.apply()
//  }
//
//}
