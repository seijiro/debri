//package com.github.seijiro.models.dao
//
//import scalikejdbc._
//import org.joda.time.{ DateTime }
//
//case class BookImpression(
//    bookImpressionId: Int,
//    bookId: Int,
//    userId: Int,
//    body: String,
//    createdAt: DateTime
//) {
//
//  def save()(implicit session: DBSession = BookImpression.autoSession): BookImpression = BookImpression.save(this)(session)
//
//  def destroy()(implicit session: DBSession = BookImpression.autoSession): Unit = BookImpression.destroy(this)(session)
//
//}
//
//object BookImpression extends SQLSyntaxSupport[BookImpression] {
//
//  override val tableName = "book_impression"
//
//  override val columns = Seq("book_impression_id", "book_id", "user_id", "body", "created_at")
//
//  def apply(bi: SyntaxProvider[BookImpression])(rs: WrappedResultSet): BookImpression = autoConstruct(rs, bi)
//  def apply(bi: ResultName[BookImpression])(rs: WrappedResultSet): BookImpression = autoConstruct(rs, bi)
//
//  val bi = BookImpression.syntax("bi")
//
//  override val autoSession = AutoSession
//
//  def find(bookImpressionId: Int)(implicit session: DBSession = autoSession): Option[BookImpression] = {
//    sql"""select ${bi.result.*} from ${BookImpression as bi} where ${bi.bookImpressionId} = ${bookImpressionId}"""
//      .map(BookImpression(bi.resultName)).single.apply()
//  }
//
//  def findAll()(implicit session: DBSession = autoSession): List[BookImpression] = {
//    sql"""select ${bi.result.*} from ${BookImpression as bi}""".map(BookImpression(bi.resultName)).list.apply()
//  }
//
//  def countAll()(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookImpression.table}""".map(rs => rs.long(1)).single.apply().get
//  }
//
//  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[BookImpression] = {
//    sql"""select ${bi.result.*} from ${BookImpression as bi} where ${where}"""
//      .map(BookImpression(bi.resultName)).single.apply()
//  }
//
//  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[BookImpression] = {
//    sql"""select ${bi.result.*} from ${BookImpression as bi} where ${where}"""
//      .map(BookImpression(bi.resultName)).list.apply()
//  }
//
//  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookImpression as bi} where ${where}"""
//      .map(_.long(1)).single.apply().get
//  }
//
//  def create(
//    bookImpressionId: Int,
//    bookId: Int,
//    userId: Int,
//    body: String,
//    createdAt: DateTime
//  )(implicit session: DBSession = autoSession): BookImpression = {
//    sql"""
//      insert into ${BookImpression.table} (
//        ${column.bookImpressionId},
//        ${column.bookId},
//        ${column.userId},
//        ${column.body},
//        ${column.createdAt}
//      ) values (
//        ${bookImpressionId},
//        ${bookId},
//        ${userId},
//        ${body},
//        ${createdAt}
//      )
//      """.update.apply()
//
//    BookImpression(
//      bookImpressionId = bookImpressionId,
//      bookId = bookId,
//      userId = userId,
//      body = body,
//      createdAt = createdAt
//    )
//  }
//
//  def batchInsert(entities: Seq[BookImpression])(implicit session: DBSession = autoSession): Seq[Int] = {
//    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
//      Seq(
//        'bookImpressionId -> entity.bookImpressionId,
//        'bookId -> entity.bookId,
//        'userId -> entity.userId,
//        'body -> entity.body,
//        'createdAt -> entity.createdAt
//      ))
//    SQL("""insert into book_impression(
//        book_impression_id,
//        book_id,
//        user_id,
//        body,
//        created_at
//      ) values (
//        {bookImpressionId},
//        {bookId},
//        {userId},
//        {body},
//        {createdAt}
//      )""").batchByName(params: _*).apply()
//  }
//
//  def save(entity: BookImpression)(implicit session: DBSession = autoSession): BookImpression = {
//    sql"""
//      update
//        ${BookImpression.table}
//      set
//        ${column.bookImpressionId} = ${entity.bookImpressionId},
//        ${column.bookId} = ${entity.bookId},
//        ${column.userId} = ${entity.userId},
//        ${column.body} = ${entity.body},
//        ${column.createdAt} = ${entity.createdAt}
//      where
//        ${column.bookImpressionId} = ${entity.bookImpressionId}
//      """.update.apply()
//    entity
//  }
//
//  def destroy(entity: BookImpression)(implicit session: DBSession = autoSession): Unit = {
//    sql"""delete from ${BookImpression.table} where ${column.bookImpressionId} = ${entity.bookImpressionId}""".update.apply()
//  }
//
//}
