package com.github.seijiro.models.dao

import scalikejdbc._

case class Purpose(
  purposeId: Int,
  message: String
)

object Purpose extends SQLSyntaxSupport[Purpose] {

  override val tableName = "purpose"

  override val columns = Seq("purpose_id", "message")

  def apply(p: SyntaxProvider[Purpose])(rs: WrappedResultSet): Purpose = autoConstruct(rs, p)
  def apply(p: ResultName[Purpose])(rs: WrappedResultSet): Purpose = autoConstruct(rs, p)

  val p = Purpose.syntax("p")

  override val autoSession = AutoSession

  def find(purposeId: Int)(implicit session: DBSession = autoSession): Option[Purpose] = {
    sql"""select ${p.result.*} from ${Purpose as p} where ${p.purposeId} = ${purposeId}"""
      .map(Purpose(p.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[Purpose] = {
    sql"""select ${p.result.*} from ${Purpose as p}""".map(Purpose(p.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Purpose.table}""".map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Purpose] = {
    sql"""select ${p.result.*} from ${Purpose as p} where ${where}"""
      .map(Purpose(p.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Purpose] = {
    sql"""select ${p.result.*} from ${Purpose as p} where ${where}"""
      .map(Purpose(p.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Purpose as p} where ${where}"""
      .map(_.long(1)).single.apply().get
  }

  def create(
    purposeId: Int,
    message: String
  )(implicit session: DBSession = autoSession): Purpose = {
    sql"""
      insert into ${Purpose.table} (
        ${column.purposeId},
        ${column.message}
      ) values (
        ${purposeId},
        ${message}
      )
      """.update.apply()

    Purpose(
      purposeId = purposeId,
      message = message
    )
  }

  def batchInsert(entities: Seq[Purpose])(implicit session: DBSession = autoSession): Seq[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'purposeId -> entity.purposeId,
        'message -> entity.message
      ))
    SQL("""insert into purpose(
        purpose_id,
        message
      ) values (
        {purposeId},
        {message}
      )""").batchByName(params: _*).apply()
  }

  def save(entity: Purpose)(implicit session: DBSession = autoSession): Purpose = {
    sql"""
      update
        ${Purpose.table}
      set
        ${column.purposeId} = ${entity.purposeId},
        ${column.message} = ${entity.message}
      where
        ${column.purposeId} = ${entity.purposeId}
      """.update.apply()
    entity
  }

  def destroy(entity: Purpose)(implicit session: DBSession = autoSession): Unit = {
    sql"""delete from ${Purpose.table} where ${column.purposeId} = ${entity.purposeId}""".update.apply()
  }

}
