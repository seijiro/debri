//package com.github.seijiro.models.dao
//
//import scalikejdbc._
//import org.joda.time.{ DateTime }
//
//case class BookReview(
//  bookId: Int,
//  userId: Int,
//  title: String,
//  body: String,
//  createdAt: DateTime,
//  updatedAt: DateTime
//)
//
//object BookReview extends SQLSyntaxSupport[BookReview] {
//
//  override val tableName = "book_review"
//
//  override val columns = Seq("book_id", "user_id", "title", "body", "created_at", "updated_at")
//
//  def apply(br: SyntaxProvider[BookReview])(rs: WrappedResultSet): BookReview = autoConstruct(rs, br)
//  def apply(br: ResultName[BookReview])(rs: WrappedResultSet): BookReview = autoConstruct(rs, br)
//
//  val br = BookReview.syntax("br")
//
//  override val autoSession = AutoSession
//
//  def find(bookId: Int, userId: Int)(implicit session: DBSession = autoSession): Option[BookReview] = {
//    sql"""select ${br.result.*} from ${BookReview as br} where ${br.bookId} = ${bookId} and ${br.userId} = ${userId}"""
//      .map(BookReview(br.resultName)).single.apply()
//  }
//
//  def findAll()(implicit session: DBSession = autoSession): List[BookReview] = {
//    sql"""select ${br.result.*} from ${BookReview as br}""".map(BookReview(br.resultName)).list.apply()
//  }
//
//  def countAll()(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookReview.table}""".map(rs => rs.long(1)).single.apply().get
//  }
//
//  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[BookReview] = {
//    sql"""select ${br.result.*} from ${BookReview as br} where ${where}"""
//      .map(BookReview(br.resultName)).single.apply()
//  }
//
//  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[BookReview] = {
//    sql"""select ${br.result.*} from ${BookReview as br} where ${where}"""
//      .map(BookReview(br.resultName)).list.apply()
//  }
//
//  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookReview as br} where ${where}"""
//      .map(_.long(1)).single.apply().get
//  }
//
//  def create(
//    bookId: Int,
//    userId: Int,
//    title: String,
//    body: String,
//    createdAt: DateTime,
//    updatedAt: DateTime
//  )(implicit session: DBSession = autoSession): BookReview = {
//    sql"""
//      insert into ${BookReview.table} (
//        ${column.bookId},
//        ${column.userId},
//        ${column.title},
//        ${column.body},
//        ${column.createdAt},
//        ${column.updatedAt}
//      ) values (
//        ${bookId},
//        ${userId},
//        ${title},
//        ${body},
//        ${createdAt},
//        ${updatedAt}
//      )
//      """.update.apply()
//
//    BookReview(
//      bookId = bookId,
//      userId = userId,
//      title = title,
//      body = body,
//      createdAt = createdAt,
//      updatedAt = updatedAt
//    )
//  }
//
//  def batchInsert(entities: Seq[BookReview])(implicit session: DBSession = autoSession): Seq[Int] = {
//    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
//      Seq(
//        'bookId -> entity.bookId,
//        'userId -> entity.userId,
//        'title -> entity.title,
//        'body -> entity.body,
//        'createdAt -> entity.createdAt,
//        'updatedAt -> entity.updatedAt
//      ))
//    SQL("""insert into book_review(
//        book_id,
//        user_id,
//        title,
//        body,
//        created_at,
//        updated_at
//      ) values (
//        {bookId},
//        {userId},
//        {title},
//        {body},
//        {createdAt},
//        {updatedAt}
//      )""").batchByName(params: _*).apply()
//  }
//
//  def save(entity: BookReview)(implicit session: DBSession = autoSession): BookReview = {
//    sql"""
//      update
//        ${BookReview.table}
//      set
//        ${column.bookId} = ${entity.bookId},
//        ${column.userId} = ${entity.userId},
//        ${column.title} = ${entity.title},
//        ${column.body} = ${entity.body},
//        ${column.createdAt} = ${entity.createdAt},
//        ${column.updatedAt} = ${entity.updatedAt}
//      where
//        ${column.bookId} = ${entity.bookId} and ${column.userId} = ${entity.userId}
//      """.update.apply()
//    entity
//  }
//
//  def destroy(entity: BookReview)(implicit session: DBSession = autoSession): Unit = {
//    sql"""delete from ${BookReview.table} where ${column.bookId} = ${entity.bookId} and ${column.userId} = ${entity.userId}""".update.apply()
//  }
//
//}
