//package com.github.seijiro.models.dao
//
//import scalikejdbc._
//
//case class BookSection(
//  bookId: Int,
//  bookSection: Int,
//  title: Option[String] = None
//)
//
//object BookSection extends SQLSyntaxSupport[BookSection] {
//
//  override val tableName = "book_section"
//
//  override val columns = Seq("book_id", "book_section", "title")
//
//  def apply(bs: SyntaxProvider[BookSection])(rs: WrappedResultSet): BookSection = autoConstruct(rs, bs)
//  def apply(bs: ResultName[BookSection])(rs: WrappedResultSet): BookSection = autoConstruct(rs, bs)
//
//  val bs = BookSection.syntax("bs")
//
//  override val autoSession = AutoSession
//
//  def find(bookId: Int, bookSection: Int)(implicit session: DBSession = autoSession): Option[BookSection] = {
//    sql"""select ${bs.result.*} from ${BookSection as bs} where ${bs.bookId} = ${bookId} and ${bs.bookSection} = ${bookSection}"""
//      .map(BookSection(bs.resultName)).single.apply()
//  }
//
//  def findAll()(implicit session: DBSession = autoSession): List[BookSection] = {
//    sql"""select ${bs.result.*} from ${BookSection as bs}""".map(BookSection(bs.resultName)).list.apply()
//  }
//
//  def countAll()(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookSection.table}""".map(rs => rs.long(1)).single.apply().get
//  }
//
//  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[BookSection] = {
//    sql"""select ${bs.result.*} from ${BookSection as bs} where ${where}"""
//      .map(BookSection(bs.resultName)).single.apply()
//  }
//
//  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[BookSection] = {
//    sql"""select ${bs.result.*} from ${BookSection as bs} where ${where}"""
//      .map(BookSection(bs.resultName)).list.apply()
//  }
//
//  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
//    sql"""select count(1) from ${BookSection as bs} where ${where}"""
//      .map(_.long(1)).single.apply().get
//  }
//
//  def create(
//    bookSection: Int,
//    title: Option[String] = None
//  )(implicit session: DBSession = autoSession): BookSection = {
//    val generatedKey = sql"""
//      insert into ${BookSection.table} (
//        ${column.bookSection},
//        ${column.title}
//      ) values (
//        ${bookSection},
//        ${title}
//      )
//      """.updateAndReturnGeneratedKey.apply()
//
//    BookSection(
//      bookId = generatedKey.toInt,
//      bookSection = bookSection,
//      title = title
//    )
//  }
//
//  def batchInsert(entities: Seq[BookSection])(implicit session: DBSession = autoSession): Seq[Int] = {
//    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
//      Seq(
//        'bookSection -> entity.bookSection,
//        'title -> entity.title
//      ))
//    SQL("""insert into book_section(
//        book_section,
//        title
//      ) values (
//        {bookSection},
//        {title}
//      )""").batchByName(params: _*).apply()
//  }
//
//  def save(entity: BookSection)(implicit session: DBSession = autoSession): BookSection = {
//    sql"""
//      update
//        ${BookSection.table}
//      set
//        ${column.bookId} = ${entity.bookId},
//        ${column.bookSection} = ${entity.bookSection},
//        ${column.title} = ${entity.title}
//      where
//        ${column.bookId} = ${entity.bookId} and ${column.bookSection} = ${entity.bookSection}
//      """.update.apply()
//    entity
//  }
//
//  def destroy(entity: BookSection)(implicit session: DBSession = autoSession): Unit = {
//    sql"""delete from ${BookSection.table} where ${column.bookId} = ${entity.bookId} and ${column.bookSection} = ${entity.bookSection}""".update.apply()
//  }
//
//}
