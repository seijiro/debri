package com.github.seijiro.models.dao

import scalikejdbc._

case class Game(
  gameId: Int,
  title: String,
  hardwares: String
)
object Game extends SQLSyntaxSupport[Game] {

  override val tableName = "game"

  override val columns = Seq("game_id", "title", "hardwares")

  def apply(g: SyntaxProvider[Game])(rs: WrappedResultSet): Game = autoConstruct(rs, g)
  def apply(g: ResultName[Game])(rs: WrappedResultSet): Game = autoConstruct(rs, g)

  val g = Game.syntax("g")

  override val autoSession = AutoSession

  def find(gameId: Int)(implicit session: DBSession = autoSession): Option[Game] = {
    sql"""select ${g.result.*} from ${Game as g} where ${g.gameId} = ${gameId}"""
      .map(Game(g.resultName)).single.apply()
  }

  def findAll()(implicit session: DBSession = autoSession): List[Game] = {
    sql"""select ${g.result.*} from ${Game as g}""".map(Game(g.resultName)).list.apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Game.table}""".map(rs => rs.long(1)).single.apply().get
  }

  def findBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Option[Game] = {
    sql"""select ${g.result.*} from ${Game as g} where ${where}"""
      .map(Game(g.resultName)).single.apply()
  }

  def findAllBy(where: SQLSyntax)(implicit session: DBSession = autoSession): List[Game] = {
    sql"""select ${g.result.*} from ${Game as g} where ${where}"""
      .map(Game(g.resultName)).list.apply()
  }

  def countBy(where: SQLSyntax)(implicit session: DBSession = autoSession): Long = {
    sql"""select count(1) from ${Game as g} where ${where}"""
      .map(_.long(1)).single.apply().get
  }

  def create(
    title: String,
    hardwares: String
  )(implicit session: DBSession = autoSession): Game = {
    val generatedKey = sql"""
      insert into ${Game.table} (
        ${column.title},
        ${column.hardwares}
      ) values (
        ${title},
        ${hardwares}
      )
      """.updateAndReturnGeneratedKey.apply()

    Game(
      gameId = generatedKey.toInt,
      title = title,
      hardwares = hardwares
    )
  }

  def batchInsert(entities: Seq[Game])(implicit session: DBSession = autoSession): Seq[Int] = {
    val params: Seq[Seq[(Symbol, Any)]] = entities.map(entity =>
      Seq(
        'title -> entity.title,
        'hardwares -> entity.hardwares
      ))
    SQL("""insert into game(
        title,
        hardwares
      ) values (
        {title},
        {hardwares}
      )""").batchByName(params: _*).apply()
  }

  def save(entity: Game)(implicit session: DBSession = autoSession): Game = {
    sql"""
      update
        ${Game.table}
      set
        ${column.gameId} = ${entity.gameId},
        ${column.title} = ${entity.title},
        ${column.hardwares} = ${entity.hardwares}
      where
        ${column.gameId} = ${entity.gameId}
      """.update.apply()
    entity
  }

  def destroy(entity: Game)(implicit session: DBSession = autoSession): Unit = {
    sql"""delete from ${Game.table} where ${column.gameId} = ${entity.gameId}""".update.apply()
  }

}
