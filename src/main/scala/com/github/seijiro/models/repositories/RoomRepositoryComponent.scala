package com.github.seijiro.models.repositories

import com.github.seijiro.models._
import com.github.seijiro.models.dao.{ Room, Users }
import com.github.seijiro.support.DatetimeSupport

trait RoomRepositoryComponent {
  val roomRepository: RoomRepository

  class RoomRepository extends DatetimeSupport {
    def findByUserName(roomId: RoomId): Option[Room] =
      Room.find(roomId)

    def countAll: Long = Room.countAll()

    def findRecentPost(paging: Paging): (List[Room], PagingResult) = {
      val rooms = Room.findAllWithPaging(paging.copy(take = paging.take + 1))
      val pagingResult = PagingResult(paging.skip != 0, rooms.length >= paging.take)

      if (pagingResult.hasNext)
        (rooms.dropRight(1), pagingResult)
      else
        (rooms, pagingResult)
    }
  }
}

