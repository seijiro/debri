//package com.github.seijiro.models.repositories
//
//import com.github.seijiro.models.BookId
//import com.github.seijiro.models.dao.Books
//import com.github.seijiro.models.entities.BookModel
//import com.github.seijiro.support.DatetimeSupport
//
//trait BookRepositoryComponent {
//  val bookRepository: BookRepository
//
//  class BookRepository extends DatetimeSupport {
//
//    def findById(bookId: BookId) =
//      Books.find(bookId).map(BookModel)
//
//    def create(title: String, isbn: Option[String], description: String, author: Option[String]) =
//      BookModel(Books.create(title, description, author, isbn.getOrElse(""), now))
//
//    def findLatestWithPaging(paging: Paging) =
//      Books.findLatestWithPaging(paging.take, paging.skip)
//  }
//}
//
