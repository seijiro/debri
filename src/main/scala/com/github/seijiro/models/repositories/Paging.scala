package com.github.seijiro.models.repositories

case class Paging(take: Int, skip: Int)
case class PagingResult(hasPrev: Boolean, hasNext: Boolean)

case class PagedObject[+A](obj: List[A], hasPrev: Boolean, hasNext: Boolean)
