package com.github.seijiro.models.repositories

import com.github.seijiro.UserNotFoundError
import com.github.seijiro.models.UserId
import com.github.seijiro.models.entities.LoginUser
import com.github.seijiro.models.dao.Users
import com.github.seijiro.support.DatetimeSupport

trait UserRepositoryComponent {
  val userRepository: UserRepository

  class UserRepository extends DatetimeSupport {
    def findByUserName(userName: String): Option[Users] =
      Users.findByUserName(userName)
    def findByUserId(userId: UserId): Option[Users] =
      Users.find(userId)
    def create(
      userName: String,
      encryptedPassword: String,
      thumbnail: String,
      profile: String,
      email: Option[String],
      homepage: Option[String]
    ): Users = {
      val n = now
      Users.create(userName, profile, thumbnail, email, homepage, encryptedPassword, n, n)
    }
  }
}

