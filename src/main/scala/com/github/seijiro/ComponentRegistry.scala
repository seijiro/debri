package com.github.seijiro

import com.github.seijiro.models.repositories.{ RoomRepositoryComponent, UserRepositoryComponent }
import com.github.seijiro.models.services._

trait ComponentRegistry
    extends UserRepositoryComponent
    with UserRegisterServiceComponent
    with UserFindServiceComponent
    with RoomRepositoryComponent {
  lazy val userRepository = new UserRepository
  lazy val userFindService = new UserFindService
  lazy val userRegisterService = new UserRegisterService
  lazy val roomRepository = new RoomRepository
}
