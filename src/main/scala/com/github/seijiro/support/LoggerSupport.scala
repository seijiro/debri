package com.github.seijiro.support

import org.slf4j._

/**
 * Created by ozw-sei on 2016/01/31.
 */
trait LoggerSupport {
  lazy val logger = LoggerFactory.getLogger(getClass)
}
