package com.github.seijiro.support

import org.joda.time.DateTime

trait DatetimeSupport {
  def now = DateTime.now
}
