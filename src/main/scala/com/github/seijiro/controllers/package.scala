package com.github.seijiro

import javax.servlet.http.HttpServletRequest

import com.github.seijiro.controllers.UserAgent._

/**
 * Created by ozw-sei on 2016/02/15.
 */
package object controllers {
  object UserAgent {
    trait UserAgent
    object Unknown extends UserAgent
    object Linux extends UserAgent
    object Mac extends UserAgent
    object Windows extends UserAgent
  }
  case class Context(flash: org.scalatra.FlashMap, loginUser: Option[models.entities.LoginUser], request: HttpServletRequest) {
    lazy val platform = request.getHeader("User-Agent") match {
      case agent if agent.contains("Mac") => Mac
      case agent if agent.contains("Linux") => Linux
      case agent if agent.contains("Win") => Windows
      case _ => Left(Unknown)
    }

  }

}
