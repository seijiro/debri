package com.github.seijiro.controllers.auth

import com.github.seijiro.ComponentRegistry
import com.github.seijiro.controllers.auth.storategies.UserPasswordStrategy
import com.github.seijiro.models.UserId
import com.github.seijiro.models.entities._
import org.scalatra.ScalatraBase
import org.scalatra.auth.{ ScentryConfig, ScentrySupport }

/**
 * Created by ozw-sei on 2016/02/15.
 */
trait AuthenticationSupport extends ScalatraBase with ScentrySupport[LoginUser] {
  self: ComponentRegistry =>

  protected def fromSession = {
    case rawId: String => userFindService.findByUserId(rawId.toInt).get
  }
  protected def toSession = {
    case usr: LoginUser => usr.userId.toString
  }

  protected val scentryConfig = (new ScentryConfig {
    override val login = "/auth/login"
  }).asInstanceOf[ScentryConfiguration]

  protected def requireLogin() = {
    if (!isAuthenticated) {
      redirect(scentryConfig.login)
    }
  }
  override protected def configureScentry = {
    scentry.unauthenticated {
      scentry.strategies(UserPasswordStrategy.name).unauthenticated()
    }
  }

  override protected def registerAuthStrategies = {
    scentry.register(UserPasswordStrategy.name, app => new UserPasswordStrategy(app))
  }
}
