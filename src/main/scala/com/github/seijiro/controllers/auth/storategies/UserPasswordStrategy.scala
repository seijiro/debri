package com.github.seijiro.controllers.auth.storategies

import com.github.seijiro.ComponentRegistry
import com.github.seijiro.models.entities.LoginUser
import com.github.seijiro.support.LoggerSupport
import org.scalatra._
import javax.servlet.http.{ HttpServletResponse, HttpServletRequest }
import org.scalatra.auth.ScentryStrategy

object UserPasswordStrategy {
  val name = "UserPassword"

}
class UserPasswordStrategy(protected val app: ScalatraBase)(implicit request: HttpServletRequest, response: HttpServletResponse)
    extends ScentryStrategy[LoginUser] with LoggerSupport with ComponentRegistry {

  override def name: String = UserPasswordStrategy.name

  private def login = app.params.get("username")
  private def password = app.params.get("password")

  override def isValid(implicit request: HttpServletRequest) = {
    logger.info("UserPasswordStrategy: determining isValid: " + (login.isDefined && password.isDefined).toString())
    login.isDefined && password.isDefined
  }

  def authenticate()(implicit request: HttpServletRequest, response: HttpServletResponse): Option[LoginUser] = {
    logger.info("UserPasswordStrategy: attempting authentication")

    // isValidでNoneでないことが確定してるので直接Getしてる
    val loginUser = userRepository.findByUserName(login.get).map(LoginUser)

    loginUser.flatMap { user =>
      if (user.Verify(password.get)) {
        logger.info("UserPasswordStrategy: login succeeded")
        Some(user)
      } else {
        logger.info("UserPasswordStrategy: login failed")
        None
      }
    }
  }

  /**
   * What should happen if the user is currently not authenticated?
   */
  override def unauthenticated()(implicit request: HttpServletRequest, response: HttpServletResponse) {
    app.redirect("/sessions/new")
  }

}
