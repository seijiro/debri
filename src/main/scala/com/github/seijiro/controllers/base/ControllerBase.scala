package com.github.seijiro.controllers.base

import com.github.seijiro.ComponentRegistry
import com.github.seijiro.controllers._
import com.github.seijiro.controllers.auth.AuthenticationSupport
import com.github.seijiro.models.entities.LoginUser
import org.scalatra._
import org.scalatra.i18n.I18nSupport

trait ControllerBase extends ScalatraServlet
    with UrlGeneratorSupport
    with I18nSupport
    with FlashMapSupport
    with AuthenticationSupport
    with ComponentRegistry {
  implicit def context: Context = Context(flash, LoginAccount, request)

  private def LoginAccount: Option[LoginUser] = scentry.userOption
}
