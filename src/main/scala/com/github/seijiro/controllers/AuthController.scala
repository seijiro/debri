package com.github.seijiro.controllers

import com.github.seijiro.{ Error, ComponentRegistry }
import com.github.seijiro.controllers.base._
import com.github.seijiro.controllers.auth.storategies.UserPasswordStrategy
import com.github.seijiro.controllers.validations.UserNameValidation
import com.github.seijiro.support.LoggerSupport
import jp.sf.amateras.scalatra.forms._
import scalaz._
import Scalaz._

class AuthController extends ControllerBase

    with FormSupport
    with ComponentRegistry
    with LoggerSupport {

  before() {
    contentType = "text/html; charset=utf-8"
  }

  case class LoginForm(userName: String, password: String)

  val loginForm = mapping(
    "username" -> text(required, maxlength(12), minlength(4)),
    "password" -> text(required, maxlength(12), minlength(5))
  )(LoginForm.apply)

  val signupForm = mapping(
    "username" -> text(required, UserNameValidation, maxlength(12), minlength(4)),
    "email" -> optional(text()),
    "password" -> text(required, maxlength(12), minlength(5)),
    "password_remind" -> text(required, maxlength(12), minlength(5))
  )(SignupForm.apply)

  case class SignupForm(userName: String, email: Option[String], password: String, passwordRemind: String) {
    def isMatch = password == passwordRemind
  }

  post("/register", signupForm) { form =>
    if (form.isMatch) {
      userRegisterService.register(form.userName, form.password) match {
        case \/-(user) =>
          flash += ("notice" -> "会員登録ありがとうございます")
          redirect(url(""))
        case -\/(userIsExistError: Error) =>
          flash += ("notice" -> userIsExistError.toString())
          redirect(url(registerUrl))
      }
    } else {
      flash += ("notice" -> "パスワードが一致しませんでした.")
      redirect(url(registerUrl))
    }
  }

  val registerUrl = get("/register") {
    if (isAuthenticated) redirect(url(""))
    html.register()
  }

  post("/login", loginForm) { form: LoginForm =>

    logger.info(s"input: username is ${form.userName}. plane password is ${form.password}.")

    userRegisterService.verify(form.userName, form.password) match {
      case \/-(isAuth) if isAuth => {
        scentry.authenticate(UserPasswordStrategy.name)
        redirect(url(""))
      }
      case \/-(isAuth) =>
        flash += ("notice" -> "password is not corrected")
        redirect(url(loginFormUrl))
      case -\/(err) =>
        flash += ("notice" -> "user not found")
        redirect(url(loginFormUrl))
    }
  }

  val loginFormUrl = get("/login") {
    if (isAuthenticated) redirect(url(""))
    html.auth()
  }

  val logout = get("/logout") {
    scentry.logout()
    redirect(url(loginFormUrl))
  }
}
