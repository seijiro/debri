package com.github.seijiro.controllers.validations

import jp.sf.amateras.scalatra.forms.Constraint
import org.scalatra.i18n.Messages

/**
 * Created by ozw-sei on 2016/02/28.
 */
object UserNameValidation extends Constraint {
  override def validate(name: String, value: String, messages: Messages): Option[String] = {
    if (!value.matches("^[a-zA-Z0-9\\-_]+$"))
      Some(s"$name contains invalid character.")
    else
      None
  }
}
