package com.github.seijiro.controllers

import com.github.seijiro.ComponentRegistry
import com.github.seijiro.controllers.base.ControllerBase
import com.github.seijiro.models.repositories.Paging
import com.github.seijiro.support.LoggerSupport
import org.scalatra.ScalatraFilter
import org.scalatra.i18n.I18nSupport

class HomeController extends ControllerBase with ComponentRegistry {
  get("/") {
    val offset = params.getOrElse("offset", "0").toInt
    val take = params.getOrElse("take", "20").toInt
    val rooms = roomRepository.findRecentPost(Paging(take, offset))

    html.index("Main")
  }

}
