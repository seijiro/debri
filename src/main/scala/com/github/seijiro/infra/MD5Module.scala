package com.github.seijiro.infra

import com.github.seijiro.support.EncryptSupport

/**
 * Created by ozw-sei on 2016/02/03.
 */
trait MD5Module extends EncryptSupport {
  def md5(text: String) =
    md.digest(text.getBytes).map("%02x".format(_)).mkString
}
