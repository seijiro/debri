package com.github.seijiro

import scalaz._
import Scalaz._

sealed trait Error {
  override def toString(): String = this match {
    case UserNameIsExistError => "target username is exist"
    case UserNotFoundError => "target user is not found"
    case PasswordAuthFailedError => "password is not corrected"
  }
}
case object UserNameIsExistError extends Error
case object UserNotFoundError extends Error
case object PasswordAuthFailedError extends Error

