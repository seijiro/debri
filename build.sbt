import com.typesafe.sbt.SbtScalariform
import scoverage.ScoverageKeys

scalikejdbcSettings

scalariformSettings

coverageEnabled := true

ScoverageKeys.coverageHighlighting := true


