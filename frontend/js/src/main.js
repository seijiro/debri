var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var Hello = (function (_super) {
    __extends(Hello, _super);
    function Hello() {
        _super.call(this);
    }
    Hello.prototype.render = function () {
        return (React.createElement("li", null, React.createElement("span", null, "test"), React.createElement("button", null, "delete")));
    };
    return Hello;
})(React.Component);
exports.Hello = Hello;
