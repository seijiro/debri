/// <reference path="../../typings/browser.d.ts" />

import * as React from "react";

export interface IMainState {
}

export interface IMainProps {}

export class Hello extends React.Component<IMainProps, IMainState>{
    constructor () {
        super();
    }
    render(){
        return (
            <li>
                <span>test</span>
                <button>delete</button>
            </li>
        );
    }
}

