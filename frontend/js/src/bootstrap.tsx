/// <reference path="../../typings/browser.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Hello} from './main';
import * as Router from 'react-router';


ReactDOM.render(<Hello />, document.getElementById("app"));