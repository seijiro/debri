var gulp = require('gulp');
var ts = require("gulp-typescript");
var browserify = require('browserify');
var source = require("vinyl-source-stream");

var sourceDir = "./js/src/**/*{ts,tsx}";
var destDir = '../src/main/webapp/assets/js/';

gulp.task('compile', function(){
    var tsconf = ts.createProject('tsconfig.json');
    tsconf.src()
        .pipe(ts(tsconf))
        .pipe(gulp.dest("dest"))
});

gulp.task('build', ["compile"], function(){
    var b = browserify({
        entries: ['./dest/js/src/bootstrap.js'],
        extensions: ['.js']
    });
    return b
        .bundle()
        .pipe(source("build.js"))
        .pipe(gulp.dest(destDir));
});


gulp.task('watch', function() {
    gulp.watch(sourceDir, ['compile']);
});


gulp.task('default', ['watch']);
