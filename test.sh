#!/usr/bin/env bash
dropdb bookreview_test;
createdb bookreview_test -O root;
sbt flywayMigrate -Dflyway.user=root -Dflyway.url=jdbc:postgresql://localhost:5432/bookreview_test -Dflyway.locations=filesystem:./db/migration
sbt test
