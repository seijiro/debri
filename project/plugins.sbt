resolvers ++= Seq(
  "sonatype releases" at "http://oss.sonatype.org/content/repositories/releases",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
)

libraryDependencies +=  "org.postgresql" % "postgresql" %  "9.4.1207.jre7"

libraryDependencies ++= Seq("org.scoverage" %% "scalac-scoverage-runtime" % "1.0.4")

addSbtPlugin("org.scalikejdbc"   % "scalikejdbc-mapper-generator" % "2.3.3")

addSbtPlugin("org.scalatra.sbt" % "scalatra-sbt" % "0.5.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.0-RC1")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0")

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

addSbtPlugin("com.typesafe.sbt" % "sbt-twirl" % "1.1.1")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.3.5")


