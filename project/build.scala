import sbt._
import Keys._
import scalikejdbc._
import org.scalatra.sbt._
import org.scalatra.sbt.PluginKeys._
import com.earldouglas.xwp.JettyPlugin
import com.typesafe.sbt.packager.archetypes.JavaAppPackaging
import scoverage.ScoverageKeys._
import scoverage.{ScoverageKeys, ScoverageSbtPlugin}
import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt._
import play.twirl.sbt.SbtTwirl
import org.flywaydb.sbt.FlywayPlugin._


object BookreviewBuild extends Build {
  val Organization = "com.github.seijiro"
  val Name = "bookreview"
  val Version = "0.1.0-SNAPSHOT"
  val ScalaVersion = "2.11.7"
  val ScalatraVersion = "2.4.0"
  val SlickJodaMapperVersion = "2.1.0"


  lazy val project = Project (
    Name,
    file("."),
    settings = ScalatraPlugin.scalatraSettings ++ SbtScalariform.scalariformSettings ++ Seq(
      organization := Organization,
      name := Name,
      version := Version,
      scalaVersion := ScalaVersion,
      resolvers += Classpaths.typesafeReleases,
      resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",
      resolvers += "amateras-repo" at "http://amateras.sourceforge.jp/mvn/",
      resolvers += "amateras-snapshot-repo" at "http://amateras.sourceforge.jp/mvn-snapshot/",

      libraryDependencies ++= Seq(
        // scalatra
        "org.scalatra" %% "scalatra" % ScalatraVersion,
        "org.scalatra" %% "scalatra-auth" % ScalatraVersion,
        "org.scalatra" %% "scalatra-json" % ScalatraVersion,
        "org.json4s"   %% "json4s-jackson" % "3.3.0.RC1",
        "jp.sf.amateras" %% "scalatra-forms" % "0.1.0",

        // utility
        "ch.qos.logback" % "logback-classic" % "1.1.3",
        "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
        "org.eclipse.jetty" % "jetty-webapp" % "9.2.14.v20151106" % "container",
        "org.eclipse.jetty" %  "jetty-plus"   % "9.1.0.v20131115",

        // core
        "com.github.tarao" %% "bullet" % "0.0.2",
        "org.scalaz" %% "scalaz-core" % "7.2.0",

        // Database
        "org.scalikejdbc" %% "scalikejdbc"        % "2.3.+",
        "org.postgresql" % "postgresql" %  "9.4.1207.jre7",
        "org.scalikejdbc" %% "scalikejdbc-syntax-support-macro" % "2.3.4",
        "org.scalikejdbc" %% "scalikejdbc-config"  % "2.3.4",

        // Testing
        "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
        "com.github.tototoshi" %% "scala-fixture" % "0.1.2" % "test",
        "org.scoverage" %% "scalac-scoverage-runtime" % "1.0.4",
        "org.scalikejdbc"   %% "scalikejdbc-test" % "2.2.+"   % "test",
        "org.seleniumhq.selenium" % "selenium-java" % "2.35.0" % "test"
      ),
      fork in Test := true,
      javaOptions in Test += "-Dconfig.resource=application.conf",
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest),
      packageOptions += Package.MainClass("JettyLauncher"),
      coverageEnabled in Test := true,
      coverageMinimum  := 70,
      coverageFailOnMinimum := false,
      coverageExcludedPackages := "com.github.seijiro.models.dao;<empty>;html;com.github.seijiro.controllers.auth;com.github.seijiro.controllers.auth.storategies;com.github.seijiro.models.dao.relations;com.github.seijiro"
    )
  )
  .enablePlugins(SbtTwirl, JavaAppPackaging, JettyPlugin)
}

